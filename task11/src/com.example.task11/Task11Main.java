package com.example.task11;

public class Task11Main {
    public static void main(String[] args) {


        int[] arr = {7, 5, 9};
        swap(arr);
        System.out.println(java.util.Arrays.toString(arr));

    }

    static int numMin(int[] arr) {
        int num=0;
        for(int i=1;i<arr.length;i++)
        {
            if(arr[num]>=arr[i])
                num=i;
        }
        return num;
    }

    static void swapInArr(int[] arr,int i,int j)
    {
        int copy=arr[i];
        arr[i]=arr[j];
        arr[j]=copy;
    }

    static void swap(int[] arr) {
        if(arr==null || arr.length==0)
            return;
        int num=numMin(arr);
        swapInArr(arr,0, num);
    }

}