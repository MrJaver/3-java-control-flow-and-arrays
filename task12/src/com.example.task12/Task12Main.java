package com.example.task12;

public class Task12Main {
    public static void main(String[] args) {

        int[] arr = {9, 11, 7, 8};
        selectionSort(arr);
        System.out.println(java.util.Arrays.toString(arr));

    }

    static void swapArr(int[] arr,int first,int second)
    {
        int copy=arr[first];
        arr[first]=arr[second];
        arr[second]=copy;
    }
    static int getNumMin(int[] arr,int start)
    {
        int numMin=start;
        for(;start<arr.length;start++) {
            if(arr[start]<arr[numMin])
                numMin=start;
        }
        return numMin;
    }
    static void partSort(int[] arr,int first)
    {
        swapArr(arr,first,getNumMin(arr,first));
    }
    static void selectionSort(int[] arr) {
        if(arr==null)return;
        for(int i=0;i<arr.length;i++)
        {
            partSort(arr,i);
        }
    }

}