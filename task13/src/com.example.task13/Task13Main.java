package com.example.task13;

import java.util.Arrays;

public class Task13Main {
    public static void main(String[] args) {

        int[] arr = {9, 1100, 7, 8};
        removeMoreThen1000(arr);
        System.out.println(java.util.Arrays.toString(arr));
    }


    static int[] removeMoreThen1000(int[] arr) {
        int[] ans={};
        if(arr==null) return null;
        for(int i=0;i<arr.length;i++)
        {
            if(arr[i]>1000)
            {
                for(int j=i;j<arr.length-1;j++)
                {
                    arr[j]=arr[j+1];
                }
                arr = Arrays.copyOf(arr,arr.length-1);

            }
        }

        return  arr;
    }

}